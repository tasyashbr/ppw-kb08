from django.db import models
from datetime import datetime, date
from django.utils import timezone
from add_hotel.models import Hotel

class Booking(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    check_in = models.DateField()
    check_out = models.DateField()
    guests = models.PositiveIntegerField()
    def __str__(self):
        return self.hotel.hotel_name + " " + self.check_in + " " + self.check_out

