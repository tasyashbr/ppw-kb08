from django.shortcuts import render
from .forms import addHotelForm
from .models import Hotel

# Create your views here.


def addHotel(request):
    if request.method == 'POST':
        form = addHotelForm(request.POST)
        if form.is_valid():
            hotel = Hotel(
                hotel_name=form.data["hotel_name"],
                city=form.data["city"],
                address=form.data["address"],
            )
            hotel.save()
            form = addHotelForm()

    else:
        form = addHotelForm()

    context = {
        'form': form,
    }
    return render(request, "addhotel.html", context)
