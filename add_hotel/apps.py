from django.apps import AppConfig


class AddHotelConfig(AppConfig):
    name = 'add_hotel'
