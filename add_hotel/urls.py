from django.urls import path
from . import views

app_name = 'add_hotel'

urlpatterns = [
    path('', views.addHotel, name="addHotel"),
]
