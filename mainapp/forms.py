from django import forms
from django.forms import ModelForm
from .models import CrowdTracker


class CreateCrowdTracker(ModelForm):
	'''
	monthAttr = {
		'type':'text',
        'class': 'form-control mb-1',
        'placeholder':'Select month'
	}
	yearAttr = {
		'type':'text',
        'class': 'form-control mb-1',
        'placeholder':'Select year'
	}
	cityAttr = {
		'type':'text',
        'class': 'form-control mt-5',
        'placeholder': 'Search by City'
	}

	city = forms.CharField(label="", max_length=50, required=True, widget=forms.TextInput(attrs=cityAttr))
	month = forms.CharField(label="", max_length=20, required=True, widget=forms.TextInput(attrs=monthAttr))
	year = forms.CharField(label="", max_length=4, required=True, widget=forms.TextInput(attrs=yearAttr))
	'''

	class Meta:
		model = CrowdTracker
		fields = ['city', 'month', 'year']

		
		widgets = {
		'city' : forms.TextInput(attrs={'class' : 'form-control mt-2', 'placeholder':'Search by city'}),
		'month': forms.Select(attrs={'class':'form-control mb-2', 'placeholder':'Select Month'}), 
		'year': forms.TextInput(attrs={'class':'form-control mb-2', 'placeholder':'Select Year'})
		}

	def __init__(self, *args, **kwargs):
		super(CreateCrowdTracker, self).__init__(*args, **kwargs)