from django.test import TestCase, Client
from django.http import HttpRequest
from .views import *
from .models import *
from .forms import *

# Create your tests here.
class CrowdTrackerTest(TestCase):
	def test_if_Crowd_Tracker_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_if_Display_url_exist(self):
		response = Client().get('/display/')
		self.assertEqual(response.status_code, 200)

	def test_Crowd_Tracker_Page(self):
		request = HttpRequest()
		response = crowdTracker(request)
		html_response = response.content.decode('utf8')
		self.assertIsNotNone(html_response)
		self.assertIn('Crowd Tracker', html_response)

	def test_view_crowd_Page(self):
		request = HttpRequest()
		response = displayByCity(request)
		html_response = response.content.decode('utf8')
		self.assertIsNotNone(html_response)
		self.assertIn('Citizi', html_response)