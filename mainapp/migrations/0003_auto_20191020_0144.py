# Generated by Django 2.2.5 on 2019-10-19 18:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0002_auto_20191019_2329'),
    ]

    operations = [
        migrations.AlterField(
            model_name='crowdtracker',
            name='month',
            field=models.IntegerField(choices=[(1, 'January'), (2, 'February'), (3, 'March'), (4, 'April'), (5, 'May'), (6, 'June'), (7, 'July'), (8, 'August'), (9, 'September'), (10, 'October'), (11, 'November'), (12, 'December')]),
        ),
        migrations.AlterField(
            model_name='crowdtracker',
            name='year',
            field=models.IntegerField(),
        ),
    ]
