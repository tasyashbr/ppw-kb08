from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import book_now
from .models import Booking
from add_hotel.models import Hotel 
from django.utils import timezone
import datetime


class BookingHotelTest(TestCase):

    def test_booking_hotel_url_is_exist(self):
        new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
        new_hotel.save()
        new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
        url = reverse('booking_hotel:book_now', args=(1,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_booking_hotel_using_template_and_exist(self):
        new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
        new_hotel.save()
        new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
        url = reverse('booking_hotel:book_now', args=(1,))
        response = self.client.get(url)
        html_response = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'booking_hotel/book_hotel_detail.html')    
        self.assertTrue(len(html_response) > 0)

    def test_process_booking_hotel_using_template(self):
        new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
        new_hotel.save()
        new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
        url = reverse('booking_hotel:book_process', args=(1,))
        response = self.client.get(url)
        html_response = response.content.decode('utf8')
        self.assertTrue(response, 'booking_hotel/book_hotel_detail.html')    

    def test_booking_hotel_using_index_func(self):
        url = reverse('booking_hotel:book_now', args=(1,))
        found = resolve(url)
        self.assertEqual(found.func, book_now)

    def test_model_can_create_new_booking(self):
        new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
        new_hotel.save()
        new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
        counting_all_available_hotel = Hotel.objects.count()
        counting_all_available_booking = Hotel.objects.count()
        self.assertEqual(counting_all_available_hotel, 1)
        self.assertEqual(counting_all_available_booking, 1)

