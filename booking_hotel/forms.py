from .models import Booking
from django import forms
import datetime
from django.core.exceptions import ValidationError

class BookingForm(forms.Form):

    customer_name = forms.CharField(label = "On Behalf Of", required = True, max_length=27, widget = forms.TextInput(
        attrs = {
            'class': 'form-control form-booking',
            'placeholder': 'Your Name'
        }
    ))

    check_in = forms.DateField(required = True, widget = forms.DateInput(
        attrs = {
            'class': 'form-control form-booking',
            'type' : 'date',
            'placeholder': 'Date in'
            
        }
    ))

    check_out = forms.DateField(required = True, widget = forms.DateInput(
        attrs = {
            'class': 'form-control form-booking',
            'type' : 'date',
            'placeholder': 'Date out'
        }
    ))
    
    guests = forms.CharField(label = "guests", required = True, max_length=27, widget = forms.TextInput(
        attrs = {
            'class': 'form-control form-booking',
            'type' : 'number',
            'placeholder': 'total people'
        }
    ))

    class Meta:
        model = Booking
        fields = ['customer-name', 'check_in', 'check-out', 'guests']

    def clean(self):
        check_in = self.cleaned_data['check_in']
        check_out = self.cleaned_data['check_out']
        
        if check_in == check_out:
             self.add_error('check_out', 'order hotel Min. 1 day')
        if check_out < check_in:
            self.add_error('check_out', "check out should be on future")
        if check_in < datetime.date.today():
             self.add_error('check_out', 'check in minimal from today')
        
        return check_in




