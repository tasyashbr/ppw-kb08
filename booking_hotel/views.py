from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .forms import BookingForm
from .models import Booking
from add_hotel.models import Hotel
from django.views.decorators.csrf import csrf_protect

response = {}

def book_now(request, hotel_id):
    hotel = get_object_or_404(Hotel, pk = hotel_id)
    response['hotel'] = hotel
    response['BookingForm'] = BookingForm
    response['isFilled'] = ""
    return render(request, 'booking_hotel/book_hotel_detail.html', response)

@csrf_protect
def book_process(request, hotel_id):
    form = BookingForm(request.POST or None)
    hotel = get_object_or_404(Hotel, pk = hotel_id)
    response['hotel'] = hotel
    if request.method == 'POST' and form.is_valid():
        booking = Booking(hotel = hotel,
        customer_name = request.POST['customer_name'],
        check_in = request.POST['check_in'],
        check_out = request.POST['check_out'],
        guests = request.POST['guests'])
        booking.save()
        return HttpResponseRedirect('/booking/')
    else:
        response['BookingForm'] = form
        return render (request, 'booking_hotel/book_hotel_detail.html', response)

def index():
    return HttpResponse("haihai")
