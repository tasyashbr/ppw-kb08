# TK 1 PPW

[![pipeline status](https://gitlab.com/tasyashbr/ppw-kb08/badges/master/pipeline.svg)](https://gitlab.com/tasyashbr/ppw-kb08/commits/master)
[![coverage report](https://gitlab.com/tasyashbr/ppw-kb08/badges/master/coverage.svg)](https://gitlab.com/tasyashbr/ppw-kb08/commits/master)



# Nama-nama Anggota Kelompok

1.  Adrian Kaiser (1806205615)
2.  Mahdia Aliyya Nuha K (1806141290)
3.  Tasya Putri Shabira (1806191616)
4.  Tolhas Parulian J (1806141473)

# Link

[HEROKU;](https://citizi.herokuapp.com)
[PERSONA;](https://docs.google.com/document/d/1n2GXcaItzObwARhzUA8ELcdEYwYAZKthwB-DaDfhaJ8/edit?usp=sharing)
[WIREFRAME;](https://wireframe.cc/citizi)
[MOCK-UP (Desktop Mode);](https://www.figma.com/file/Yp2QI7AnCo4o8PSBYPcGJ8/Citizi)
[MOCK-UP (Phone Mode);](https://marvelapp.com/cj53a87)

# Informasi Branch dan Pembagian Tugas
**Branch**
*  decoff: Adrian Kaiser
*  tasyashbr: Tasya Putri Shabira
*  mahdia/add_hotel: Mahdia Aliyya Nuha K
*  booking_hotel: Tolhas Parulian J

**Fitur**
*  [mainapp] Crowd tracker: Adrian Kaiser
*  [add_hotel] Add hotel: Mahdia Aliyya Nuha K
*  [bookinghotel] Search for booking hotel: Tasya Putri Shabira
*  [booking_hotel] Booking and booking details : Tolhas Parulian J

# Tentang Aplikasi

Aplikasi Citizi memberikan informasi mengenai jumlah populasi masyarakat di
suatu kota pada waktu tertentu dengan data yang nantinya (pada beberapa tahun yang akan datang)
dikumpulkan dari situs penyedia travel dan penginapan seperti traveloka.com, tiket.com,
pegipegi.com, dan lain-lain. Data-data yang dikumpulkan meliputi jumlah pendatang di suatu
kota pada jangka waktu tertentu untuk memperkirakan kepadatan kota tersebut.

Dengan adanya informasi yang tersedia, baik masyarakat umum maupun organisasi dan
pemerintah dapat memantau tingkat kepadatan suatu kota dengan tujuan masing-masing.
Untuk lebih lengkapnya dapat melihat persona website Citizi di [PERSONA](https://docs.google.com).

Future updates will include:
* Integrasi aplikasi Citizi dengan platform penyedia data
* Notifikasi ketika terdapat lonjakan kepadatan pada beberapa hari mendatang berdasarkan analisis data dan GPS pengguna
* Visualisasi data yang lebih atraktif
* Prediksi dan suggestion untuk tiap pengguna
* Fitur user interface berbasis AI untuk grab data pengguna

# Fitur Aplikasi Citizi (current)
* Crowd tracking
* Add hotel (future update in 10 years: get data from another platform, so no need to fill the form manually)
* Search for hotel by city
* Book hotel
* See booking details