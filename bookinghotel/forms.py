from django import forms
from .models import SearchHotel


class SearchHotelForm(forms.Form):
    id_search = forms.CharField(max_length=50, required=True)
