from django.shortcuts import render, redirect
from add_hotel.models import Hotel
from .models import SearchHotel
from .forms import SearchHotelForm

# Create your views here.


def booking_hotel(request):
    if request.method == 'POST':
        form = SearchHotelForm(request.POST)
        if form.is_valid():
            id_search = request.POST['id_search']
            hotel_by_city = Hotel.objects.filter(city=id_search)
            return render(request, 'bookinghotel.html', {
                'hotel_by_city': hotel_by_city
            })
    return render(request, "bookinghotel.html")
