from django.apps import AppConfig


class BookinghotelConfig(AppConfig):
    name = 'bookinghotel'
