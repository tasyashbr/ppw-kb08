from django.urls import path
from .views import booking_hotel

urlpatterns = [
    path('', booking_hotel, name="booking"),
]
