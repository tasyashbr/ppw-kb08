from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import *
from .forms import *
from . import views
import unittest

# Create your tests here.


class FormTest(TestCase):
    def test_booking_page_response(self):
        self.response = Client().get('/booking/')
        self.assertEqual(self.response.status_code, 200)

    def test_booking_url_name(self):
        self.found = resolve('/booking/')
        self.assertEqual(self.found.url_name, 'booking')

    def test_form_is_exist(self):
        self.response = Client().get('/booking/')
        self.assertIn('</form>', self.response.content.decode())

    def test_template_is_exist(self):
        self.response = Client().get('/error')
        self.assertEqual(self.response.status_code, 404)

    def test_button_is_exist(self):
        self.response = Client().get('/booking/')
        self.assertIn('</button>', self.response.content.decode())

    def test_form_function(self):
        self.found = resolve('/booking/')
        self.assertEqual(self.found.func, views.booking_hotel)
